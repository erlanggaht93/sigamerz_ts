import { TypeColor } from "./typeColor"

type TypeUser =  {
    src?: string,
    name?: string,
    desc?: string,
    color?: TypeColor
}

export type {TypeUser}
