'use client'
import MyCardGlass from "@/app/components/card/cardglass"
import { Button, ScrollShadow } from "@nextui-org/react"
import { MyCardGlassSmall } from "@/app/components/card/cardglassSmall"
import useRequestGames from "@/app/services/hooks/ReactQuery/useRequestGames"
import { getGames } from "@/app/services/API/listgame"
import { useCallback, useEffect, useRef, useState } from "react"
import { useIntersectionObserver } from "@/app/services/hooks/useIntersectionObserver"
import { usePathname, useRouter, useSearchParams } from "next/navigation"

export type TypeDataGames = {
  albumId: number,
  id: number,
  title: string,
  url: string,
  thumbnailUrl: string,
  slice: (args1: number, args2: number) => TypeDataGames[]
}

function page() {
  const router = useRouter()
  const { query } = useRequestGames()
  const { data: dataGames } = query({
    key: ["GAMES"], queryFunction: getGames, option: {
      staleTime: 20000,
      cacheTime: 50000,
    }
  })

  // Create query string
  const pathname = usePathname()
  const searchParams = useSearchParams()!
  const createQueryString = useCallback((name: string, value: string | number) => {
    const params = new URLSearchParams(searchParams)
    params.set(name, value.toString())

    return params.get('limit')
  }, [searchParams])


  // Intersection Observer
  const ref = useRef<HTMLDivElement>(null);
  const entry = useIntersectionObserver(ref, {
    rootMargin: "0%"
  })
  const isVisible = !!entry?.isIntersecting

  // Data Limit 20
  const [resultLimit, setResultLimit] = useState<TypeDataGames[]>([])
  const [limit, setLimit] = useState<number>(0)
  useEffect(() => {
    // if(angka === 20) handleSuccess(dataGames)
    if (isVisible === true) {
      const queryNumber = createQueryString('limit', 20)
      setLimit((prev) => prev + parseInt(queryNumber))
      setTimeout(() => {
        handleSuccess(dataGames)
      }, 900)
    }
  }, [dataGames, isVisible])

  function handleSuccess(data: TypeDataGames) {
    const limitArray = data?.slice(0, limit)
    setResultLimit(limitArray)
  }
  console.log(limit)

  return (
    <main>
      <ScrollShadow className="grid grid-cols-2 md:grid-cols-5 px-4 gap-3 mx-auto  max-h-[480px] ">
        {[1, 2, 3, 4, 5, 6, 7, 8].map((m: number, i: number) => {
          return <div key={i}><MyCardGlassSmall /></div>
        })}
      </ScrollShadow>

      <div className="h-full">
        <div className="flex px-4 justify-center mt-16">
          <MyCardGlass src="https://wallpaperset.com/w/full/0/2/c/118425.jpg" otherClass="-rotate-6 w-[680px] h-[430px] opacity-60 hover:opacity-100" />
          <div className="w-20"></div>
          <MyCardGlass src="https://wallpaperset.com/w/full/a/9/8/118423.jpg" otherClass="w-[920px] h-[360px] opacity-60 hover:opacity-100" />
        </div>
        <div className="relative w-full px-6 md:px-12 mt-6">
          <MyCardGlass src="https://wallpapers.com/images/high/genshin-impact-background-1at4i3bbwd6586f4.webp" otherClass="w-full md:w-[870px] h-[380px]  md:absolute md:left-20 md:top-6 opacity-60 hover:opacity-100" />

          <MyCardGlass src="https://wallpapercave.com/wp/PQEWu3H.jpg" otherClass="w-full md:w-[750px] h-[380px]  rotate-3 md:absolute md:right-32 md:top-12  opacity-60 hover:opacity-100" />
        </div>
      </div>

      <div className="mt-[500px] p-3">
        <h1 className="text-3xl uppercase mb-6">List Game</h1>

        <ScrollShadow className="w-[50%] h-[60vh]" hideScrollBar>
          <div role="listgame" className="py-1">
            <ul>
              {resultLimit?.map((m: TypeDataGames, i: number) => {
                return <li key={m.id} className="p-2 flex items-center justify-between">
                  <p>
                    {m.id}. {m.title}
                  </p>
                  <Button>Download</Button>
                </li>
              })}
            </ul>
          </div>
          <div ref={ref} className="text-white/50 p-1 px-2">{isVisible && `${resultLimit?.length + 1}. Loading ...`}</div>
        </ScrollShadow>
      </div>
    </main>)
}


export default page;


