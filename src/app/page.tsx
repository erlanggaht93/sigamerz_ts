'use client'
import Link from "next/link";
import MainContainer from "./components/mainContainer";
import MyUser from "./components/user/user";
import { Button } from "@nextui-org/react";


export default function Home() {

  return (

    <>
      <div className="fixed z-50 right-6 top-6">
        <MyUser name="Erlanggaht" desc="admin" src="https://i.pravatar.cc/150?u=a04258a2462d826712d" />
      </div>

      <MainContainer otherClass='relative flex justify-center items-center h-screen w-screen'>
        <div id="bgMain"></div>
        <div className="z-10 flex flex-col items-center text-center">
          <h1 role="titleMain" className="md:text-titlebig text-6xl font-Signika md:leading-[120px]  uppercase drop-shadow">SiGamerz</h1>
          <h3 className="md:text-5xl text-2xl md:max-w-[1290px] py-2 font-semibold drop-shadow">Welcome to <span className="text-primary">sigamerz</span>, get a list of interesting and newest games</h3>
          <p className="pt-3 md:pt-6text-lg md:text-xl drop-shadow">visit my social media <Link href={"https://instagram.com/erlanggaht93"} className="underline">Erlanggaht93</Link></p>
          <Link href={"/listgame"}>
            <Button className="border-2 border-white mt-4 px-6 bg-transparent shadow text-sm " variant="solid" radius="md" endContent={<ArrowIcon />}>
              List Game
            </Button>
          </Link>
        </div>
      </MainContainer>

      <div className="fixed z-50 bottom-6 left-6">

      </div>
    </>
  )
}


export const ArrowIcon = () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 25 25" width={24} height={24}><path style={{ "fill": "#fff" }} d="m17.5 5.999-.707.707 5.293 5.293H1v1h21.086l-5.294 5.295.707.707L24 12.499l-6.5-6.5z" data-name="Right" /></svg>
  );
};