import { Button, Card, CardFooter, Image } from "@nextui-org/react"

export const MyCardGlassSmall = ({
  src = "https://img.itch.zone/aW1hZ2UvMTIxNjU4LzU2MDQxMC5wbmc=/original/hCUwLQ.png",
} : {
  src?: string
}) => {
    return <Card
      isFooterBlurred
      radius="sm"
      className="border-none bg-body opacity-90 hover:opacity-100"
      >
      <Image
        alt="Woman listing to music"
        className="object-cover w-full h-[220px]"
        src={src}
        radius="none"
  
      />
      <CardFooter className="justify-between before:bg-darkness border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
        <p className="text-tiny text-white/80">Coming soon.</p>
        <Button className="text-tiny text-primary bg-black/20" variant="solid" color="primary" radius="sm" size="sm">
          Notify me
        </Button>
      </CardFooter>
    </Card>
  
  } 