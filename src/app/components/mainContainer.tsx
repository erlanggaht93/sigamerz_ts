import {ReactNode,HTMLAttributes} from 'react'

interface MainContainerProps extends HTMLAttributes<HTMLDivElement> {
    children: ReactNode;
    otherClass?: string;
    otherAttr?: object;
  }

export default function MainContainer({children, otherClass = '', ...otherAttr} : MainContainerProps) {
    return (
        <main 
        className={`px-3 ${otherClass}`}
        {...otherAttr}
        >
            {children}
        </main>
    )
}
