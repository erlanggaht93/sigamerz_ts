'use client'
import { TypeUser } from '@/app/types/typeUser'
import {  Dropdown, DropdownItem, DropdownMenu, DropdownTrigger, User } from '@nextui-org/react'

export default function MyUser({
    src = "",
    name = 'Root', 
    desc = 'admin',
    color = "primary"
} : TypeUser ) {
    return (
        <Dropdown placement="bottom-start" backdrop="blur">
          <DropdownTrigger>
            <User
              as="button"
              avatarProps={{
                isBordered: true,
                src: src,
              }}
              className="transition-transform"
              description={<span className='text-primary'>{desc}</span>}
              name={name}
            />
          </DropdownTrigger>
          <DropdownMenu aria-label="User Actions" variant="flat">
            <DropdownItem key="profile" className="h-14 gap-2">
              <p className="font-bold">Signed in as</p>
              <p className="font-bold">@{name}</p>
            </DropdownItem>
            <DropdownItem key="settings">
              My Settings
            </DropdownItem>
            <DropdownItem key="logout" color={color}>
              Log Out
            </DropdownItem>
          </DropdownMenu>
        </Dropdown>
    )
}
