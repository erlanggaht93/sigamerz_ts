import axios, { AxiosResponse } from 'axios';

type TypeGetGames <T> = () => Promise<T>;

const getGames: TypeGetGames<any> = async () => {
    try {
        const response: AxiosResponse<any>  = await axios('https://jsonplaceholder.typicode.com/photos')
        return response.data
    } catch (error) {
        throw error
    }
}
    

export {getGames};
export type {TypeGetGames}