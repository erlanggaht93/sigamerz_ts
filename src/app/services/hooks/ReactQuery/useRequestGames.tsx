'use client'
import { useQuery, useQueryClient } from '@tanstack/react-query'
import { TypeGetGames } from '../../API/listgame'

type TypeReturnUseRequestGames = {
    query: any,
    mutation?: any
}

type TypePropsUseRequestGames = {
    key: (string | number)[],
    queryFunction: () => Promise<TypeGetGames<any>>,
    option?: any
}

function useRequestGames(): TypeReturnUseRequestGames {
    const queryClient = useQueryClient()
    const query = ({
        key,
        queryFunction,
        option = { }
    }: TypePropsUseRequestGames) => {
        return useQuery({
            queryKey: key,
            queryFn: queryFunction,
            ...option
        })
    }

    const mutation = ({
        key,
        queryFunction,
        option: { }
    }: TypePropsUseRequestGames) => {
    }

    return {
        query,
        mutation
    }
}

export default useRequestGames