import type { Config } from 'tailwindcss'
const {nextui} = require("@nextui-org/react");
const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: { 
    fontFamily: { 
        "Signika": ['Audiowide', 'sans-serif'] 
    },
    fontSize : {
      titlebig: "7.8rem",
      desc: ""
    } 
}, },
  plugins: [nextui({
    themes: {
      light: {},
      dark: {
        colors : {
          body: { DEFAULT : "#24272d"},
          primary: {DEFAULT: "#fbbf24"},
          white: {DEFAULT: "#eee"},
          darkness: {DEFAULT:"#222"}
        }
      },
    },

  })],
}
export default config
